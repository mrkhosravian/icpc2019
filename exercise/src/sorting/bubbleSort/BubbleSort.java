package sorting.bubbleSort;

public class BubbleSort {

    public static void main(String[] args) {
        int[] a = new int[]{5,1,204,14};
        for (int i : a ) {
            System.out.print(i + "\t");
        }
        bubbleSort(a);
        System.out.println();
        for (int i : a ) {
            System.out.print(i + "\t");
        }
    }

    public static void bubbleSort(int[] a) {
        for (int i = a.length - 1; i >= 1 ; i--) {
            for (int j = 0; j <= i - 1; j++) {
                if (a[j] > a[j+1]) {
                    int swap = a[j];
                    a[j] = a[j+1];
                    a[j+1] = swap;
                }
            }
        }
    }
}
