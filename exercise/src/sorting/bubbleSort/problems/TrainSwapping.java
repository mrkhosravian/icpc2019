package sorting.bubbleSort.problems;

import annotations.Persian;
import annotations.URL;

import java.util.Scanner;

@Persian("جابجایی واگن ها")
@URL("https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=235")

public class TrainSwapping {

    public static void main(String[] args) {


        String testCases = "3\n" +
                "3\n" +
                "1 3 2\n" +
                "4\n" +
                "4 3 2 1\n" +
                "2\n" +
                "2 1";

        Scanner scanner = new Scanner(testCases);
        int items = scanner.nextInt();
        while (items --> 0) {
            int[] array = new int[scanner.nextInt()];
            for (int i = 0; i < array.length; i++) {
                array[i] = scanner.nextInt();
            }
            int swapCount = bubbleSortWithSwapCounter(array);
            System.out.println("Optimal train swapping takes " + swapCount + " swaps");
        }

    }


    public static int bubbleSortWithSwapCounter(int[] a) {
        int swapCount = 0;
        for (int i = a.length - 1; i >= 1; i--) {
            for (int j = 0; j <= i - 1; j++) {
                if (a[j] > a[j + 1]) {
                    swapCount++;
                    int swap = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = swap;
                }
            }
        }
        return swapCount;
    }

}
