package sorting.bubbleSort.problems;

import annotations.Persian;
import annotations.URL;

import java.util.Arrays;
import java.util.Scanner;

import static sorting.bubbleSort.problems.TrainSwapping.bubbleSortWithSwapCounter;

@Persian("دی ان ای")
@URL("https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=553")

public class DNASorting {
    public static void main(String[] args) {
        String testCases = "1\n" +
                "\n" +
                "10 6\n" +
                "AACATGAAGG\n" +
                "TTTTGGCCAA\n" +
                "TTTGGCCAAA\n" +
                "GATCAGATTT\n" +
                "CCCGGGGGGA\n" +
                "ATCGATGCAT";

        Scanner sc = new Scanner(testCases);
        int items = sc.nextInt();
        while (items-- > 0) {

            int n = sc.nextInt();
            int m = sc.nextInt();

            Sequence[] sequences = new Sequence[m];

            for (int i = 0; i < m; i++) {
                String seq = sc.next();
                sequences[i] = new Sequence(i, seq);
            }

            Arrays.sort(sequences);

            for (int j = 0; j < m; j++) {
                System.out.println(sequences[j].seq);
            }
        }
    }


    static int getSwaps(String sequence) {
        int ans = 0, n = sequence.length();
        char[] seq = sequence.toCharArray();
        for (int i = n-1; i >= 0 ; i--) {
            for (int j = 0; j < i; j++) {
                if (seq[j] > seq[j+1]) {
                    char tmp = seq[j];
                    seq[j] = seq[j+1];
                    seq[j+1] = tmp;
                    ans++;
                }
            }
        }
        return ans;
    }

    static class Sequence implements Comparable<Sequence> {

        int pos;
        String seq;

        public Sequence(int pos, String seq) {
            this.pos = pos;
            this.seq = seq;
        }

        @Override
        public int compareTo(Sequence that) {
            int swaps1 = getSwaps(this.seq);
            int swaps2 = getSwaps(that.seq);
            if (swaps1 - swaps2 != 0)
                return swaps1 - swaps2;
            else
                return this.pos - that.pos;
        }
    }

}
