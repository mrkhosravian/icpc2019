package mathematics.algorithms.LowestCommonMultiple;

import annotations.Persian;
import annotations.URL;
import mathematics.algorithms.GreatestCommonDivisor.GreatestCommonDivisor;

@Persian("کوچک ترین مضرب مشترک")
@URL("https://fa.wikipedia.org/wiki/%DA%A9%D9%88%DA%86%DA%A9%E2%80%8C%D8%AA%D8%B1%DB%8C%D9%86_%D9%85%D8%B6%D8%B1%D8%A8_%D9%85%D8%B4%D8%AA%D8%B1%DA%A9")
public class LowestCommonMultiple {

    public static void main(String[] args) {
        System.out.println("lcm(2,3) = " + lcm(2, 3));
        System.out.println("lcm(15,25) = " + lcm(15, 25));
        System.out.println("lcm(7398,2877) = " + lcm(7398,2877));
    }

    public static int lcm(int a, int b) {
        int gcd = GreatestCommonDivisor.gcd(a,b);
        int lcm = (a * b) / gcd;
        return lcm;
    }

}
