# Lowest Common Divisor
### lcm is relative to gcd

#### Formula
```
   a x b = LCM(a, b) * GCD (a, b)

   LCM(a, b) = (a x b) / GCD(a, b)
```

## Application of LCM
* to find a synchronization time between two traffic light , if traffic light A display green color every 3 minutes and traffic light B display green color every 2 minutes, then every 6 minutes, both traffic light will display green color at the same time 