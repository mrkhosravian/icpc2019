package mathematics.algorithms.GreatestCommonDivisor;

import annotations.Persian;
import annotations.URL;

@Persian("بزرگ ترین مقسوم علیه مشترک")
@URL("https://fa.wikipedia.org/wiki/%D8%A8%D8%B2%D8%B1%DA%AF%E2%80%8C%D8%AA%D8%B1%DB%8C%D9%86_%D9%85%D9%82%D8%B3%D9%88%D9%85%E2%80%8C%D8%B9%D9%84%DB%8C%D9%87_%D9%85%D8%B4%D8%AA%D8%B1%DA%A9")
public class GreatestCommonDivisor {

    public static void main(String[] args) {
        System.out.println(gcd(7398, 2877));
        System.out.println(gcd_uva(2877, 7398));
    }

    /**
     * @param a
     * @param b
     * @return Greatest Common Divisor
     */
    public static int gcd(int a, int b) {
        if (a == 0)
            return b;

        return gcd(b % a, a);
    }


    /**
     * Bitwise method
     * @param a
     * @param b
     * @return Greatest Common Divisor
     */
    public static int gcd_uva(int a, int b) {
        while (b > 0) {
            a = a % b;
            a = a ^ b;
            b = b ^ a;
            a = a ^ b;
        }
        return a;
    }


}
