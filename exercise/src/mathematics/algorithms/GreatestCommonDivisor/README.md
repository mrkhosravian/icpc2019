# Greates Common Divisor
### Euclidean algorithms

#### Formula
```gcd(a,b) = last non zero reminder```

---

### example =
```
gcd(7398,2877) = ?

7389 = 2(2877) + 1644
gcd(7398,2877) = gcd(2877,1644)

2877 = 1(1233) + 411 // last non zero reminder
gcd(2877,1644) = gcd(1233,411)
 
1233 = 3(411) + 0
```