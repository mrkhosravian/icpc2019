package mathematics.algorithms.fernatLittleTest;

import java.math.BigInteger;

public class FermatLittleTest {

    public static void fermatTest(int a, int p) {
        boolean isFermat = BigInteger.valueOf(a).pow(p-1).remainder(BigInteger.valueOf(p)).equals(BigInteger.ONE);
        if (isFermat) System.out.println(p + " is maybe a Prime number or maybe Carmichael Number");
        else System.out.println(p + " is not a prime number");
    }

    private static void isPrime(int test) {
        fermatTest(2, test);
    }

    public static void main(String[] args) {

        fermatTest(2, 17);
        fermatTest(9, 17);
        fermatTest(39, 17);
        fermatTest(100, 17);

        int[] testCases = new int[]{ 17, 65634631, 232, 52633};

        for (int test : testCases ) {
            isPrime(test);
        }
    }
}
