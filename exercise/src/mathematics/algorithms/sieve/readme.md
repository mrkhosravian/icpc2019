# Sieve of Eratosthenes
complexity : O(n loglogn)

CodeChef problems:
[The First Cube](https://www.codechef.com/problems/FCUBE)
- sieve for primes
- 10^9+7 mod
- [gcd](../GreatestCommonDivisor/README.md) 


---

##### mod of 10^9+7
How to use mod of 10e9+7 :
```
a + b = (a % 10e9+7) + (b % 10e9+7)
a - b = (a % 10e9+7) - (b % 10e9+7)
a * b = (a % 10e9+7) * (b % 10e9+7)
```