package mathematics.algorithms.sieve;

import java.math.BigInteger;
import java.util.Arrays;

public class SieveofEratosthenes {

    // check if 10^9+7 is prime
    public static void main(String[] args) {
        int num = 1000000007;
        // using sieve
        long start = System.currentTimeMillis();
        boolean[] primes = generate(num);
        if (primes[num]) System.out.println("sieve = " + num + " is a prime");
        else System.out.println("sieve = " + num + " is not a prime");
        System.out.println("sieve = " + (System.currentTimeMillis() - start) + "ms");

        // using big integer
        start = System.currentTimeMillis();
        BigInteger _Num = BigInteger.valueOf(num);
        boolean isPrime = _Num.isProbablePrime(1);
        if (isPrime) System.out.println("BigInteger = " + num + " is a prime");
        else System.out.println("BigInteger = " + num + " is not a prime");
        System.out.println("BigInteger = " + (System.currentTimeMillis() - start) + "ms");
    }

    static boolean[] generate(int max){
        boolean[] primes = new boolean[max+1];
        Arrays.fill(primes,true);
        for (int p = 2; p*p <= max; p++) {
            if (primes[p]){
                for (int i = p*p; i <= max; i+=p) {
                    primes[i] = false;
                }
            }
        }
        return primes;
    }
}
