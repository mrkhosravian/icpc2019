# Searching

### Searching algorithms

|   | name | growth function | unbound | need to be sorted | notes |
|---|------|-----------------|-------|---------------------|-------|
|1| Linear Search|O(n)| true | false | good for unsorted lists|
|2| Binary Search|O(logn)|false|true|fastest algorithm for searching|
|3| Jump Search  |O(sqrt(n))|true|true|jumps sqrt(n) elements at a time|
|4| Interpolation Search|O(n)/Ω(loglogn)|false|true| it performs better than binary search if the values are uniformly distributed|
|5| Exponential Search|O(logn)|true|true| makes binary search unbound|

###### Optimized binary search
```c
int BinarySearch(int A[], int l, int r, int key) 
{ 
    int m;
    while( r - l > 1 ) { 
        m = l + (r-l)/2;
        if( A[m] <= key )  l = m; 
        else r = m; 
    } 
  
    if( A[l] == key ) return l; 
    else if( A[r] == key ) return r; 
    else return -1; 
} 
```

###### Exponential search
```c
int exponentialSearch(int arr[], int n, int x) 
{ 
    if (arr[0] == x) return 0; 
  
    int i = 1; 
    while (i < n && arr[i] <= x)  i = i*2; 
  
    return binarySearch(arr, i/2, min(i, n), x); 
} 
```

## Binary Search trees

Binary Search Tree is a node-based binary tree data structure which has the following properties:

- The left subtree of a node contains only nodes with keys lesser than the node’s key.
- The right subtree of a node contains only nodes with keys greater than the node’s key.
- The left and right subtree each must also be a binary search tree.

|   | name | self-balancing | insert  | search | notes |
|---|------|----------------|---------|--------|-------|
| 1 | BST  | false          | O(h)    | O(n)/Ω(logn)| normal binary search tree|
| 2 | AVL Tree| true        | O(logn) | O(logn)| advanced binary search tree   |
| 3 | Splay Tree| true      | O(logn) | O(logn)/Ω(1)| the most popular advanced binary search tree|
| 4 | B-Tree| true          | O(logn) | O(logn)| used for File based storage|
| 5 | Red-Black Tree | true | O(logn) | O(logn)| used in java's `TreeMap` & `TreeSet`|

###### Binary Search Tree
```java
class BST {
  
       /* Class containing left and right child of current node and key value*/
        class Node { 
            int key; 
            Node left, right; 
      
            public Node(int item) { 
                key = item; 
                left = right = null; 
            } 
        } 
        
        Node root; 
      
        // This method mainly calls insertRec() 
        void insert(int key) { 
           root = insertRec(root, key); 
        } 
          
        
        Node insertRec(Node root, int key) { 
      
            /* If the tree is empty, return a new node */
            if (root == null) { 
                root = new Node(key); 
                return root; 
            } 
      
            /* Otherwise, recur down the tree */
            if (key < root.key) 
                root.left = insertRec(root.left, key); 
            else if (key > root.key) 
                root.right = insertRec(root.right, key); 
      
            /* return the (unchanged) node pointer */
            return root; 
        } 
        
            // This method mainly calls deleteRec() 
            void deleteKey(int key) 
            { 
                root = deleteRec(root, key); 
            } 
          
            
            Node deleteRec(Node root, int key) 
            { 
                /* Base Case: If the tree is empty */
                if (root == null)  return root; 
          
                /* Otherwise, recur down the tree */
                if (key < root.key) 
                    root.left = deleteRec(root.left, key); 
                else if (key > root.key) 
                    root.right = deleteRec(root.right, key); 
          
                // if key is same as root's key, then This is the node 
                // to be deleted 
                else
                { 
                    // node with only one child or no child 
                    if (root.left == null) 
                        return root.right; 
                    else if (root.right == null) 
                        return root.left; 
          
                    // node with two children: Get the inorder successor (smallest 
                    // in the right subtree) 
                    root.key = minValue(root.right); 
          
                    // Delete the inorder successor 
                    root.right = deleteRec(root.right, root.key); 
                } 
          
                return root; 
            } 
          
            int minValue(Node root) 
            { 
                int minv = root.key; 
                while (root.left != null) 
                { 
                    minv = root.left.key; 
                    root = root.left; 
                } 
                return minv; 
            } 
      
        // This method mainly calls InorderRec() 
        void inorder() { 
           inorderRec(root); 
        } 
        
        void inorderRec(Node root) { 
            if (root != null) { 
                inorderRec(root.left); 
                System.out.println(root.key); 
                inorderRec(root.right); 
            } 
        }
}
```
#### Tree traversal
Breadth First or Level Order Traversal : 1 2 3 4 5
Depth First Traversals:
1. Inorder (Left, Root, Right) : 4 2 5 1 3
2. Preorder (Root, Left, Right) : 1 2 4 5 3
3. Postorder (Left, Right, Root) : 4 5 2 3 1

