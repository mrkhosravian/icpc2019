package searching;

import java.util.Arrays;

public class OptimizedBinarySearch {

    public static void main(String[] args) {
        int[] array = {1,4,8,9,12,15,21,41,52,58,62,65,69,72,79,80,84,88,95,96,100};

        long start = System.nanoTime();
        int result = binarySearch(array,0,array.length-1,52);
        System.out.println("optimized time = " + (System.nanoTime() - start)+ "ns");
        System.out.println("result = " + result);
        start = System.nanoTime();
        result = Arrays.binarySearch(array,0,array.length-1,52);
        System.out.println("java binary search time = " + (System.nanoTime() - start)+ "ns");
        System.out.println("result = " + result);
    }

    static int binarySearch(int A[], int l, int r, int key)
    {
        int m;
        while( r - l > 1 ) {
            m = l + (r-l)/2;
            if( A[m] <= key )  l = m;
            else r = m;
        }

        if( A[l] == key ) return l;
        else if( A[r] == key ) return r;
        else return -1;
    }
}
