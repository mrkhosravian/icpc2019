package searching;

public class InfinitySearch {
    // find x such that when f(x) is more than 0
    static int f(int x){
        return x*x-2145*x-541;
    }

    public static void main(String[] args) {
        // naive approach
        long start = System.currentTimeMillis();
        int i = 1;
        while (f(i)<0) i++;
        System.out.println("i = " + i);
        System.out.println("linear approach = " + (System.currentTimeMillis() - start) + "ms");

        // exponential approach
        start = System.currentTimeMillis();
        i = 1;
        while (f(i)<0) i = i*2;
        int l = i/2;
        int r = i;
        int x = l + 1;
        while (f(x)<0) x++;
        System.out.println("i = " + x);
        System.out.println("exponential approach = " + (System.currentTimeMillis() - start) + "ms");
    }
}
