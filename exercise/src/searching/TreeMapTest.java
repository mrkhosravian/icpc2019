package searching;

import java.util.TreeMap;

public class TreeMapTest {
    public static void main(String[] args) {
        java.util.TreeMap<Integer,Integer> redBlack = new java.util.TreeMap<>();
        for (int i = 100; i > 0 ; i--) {
            redBlack.put(i,i);
        }
        System.out.println(redBlack.toString());
    }
}