# icpc2019

| Day     | Meal    |
| --------|---------|
| problems solved    | 10 |
| algorithms learned | 0  |

## Time Table

| week day     | time range    |
| ------------|---------|
| saturday    | 12 - 4 |
| sunday | 12 - 4  |
| monday    | 12 - 4 |
| thueseday | 12 - 4  |
| wednesday    | 12 - 4 |
| thursday | 12 - 4  |
| friday | 12 - 4  |

<p>optimal total hours: 10h</p>

## TODO
* 16th Sharif, Problem C -> Finding divisions fastest way
* Least chosen sets that contains all members algorithm
* Primes, BitWiseSieve algorithm -> Learn how to use bits as storage; change and retrieve bits. may become handy :)
* Community detection algorithms e.g. infoMap

### Problems

| Problem     | Url    |
| --------|---------|
| phere online judge    | https://www.spoj.com/problems/classical/ |
| UVa online judge | https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8  |


### Contests

| Problem     | Url    |
| --------|---------|
| running online constests    | http://coj.uci.cu/contest/running.xhtml |
  

## Formulas

| Name     | Formula    |
| --------|---------|
| Pascal Triangle element   | ```c = c * (line - i) / i``` |
| Big mod   | ```(A*B*C) mod N == ((A mod N) * (B mod N) * (C mod N)) mod N``` |
| Primes   | ```Fermat's Theorm``` |